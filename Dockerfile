FROM postgres:12
ENV POSTGRES_DB: newpg
ENV POSTGRES_USER: fpltech
ENV POSTGRES_PASSWORD: 1FPLabs23

FROM amazoncorretto:8
COPY . /root/hello_jpos
WORKDIR /root/hello_jpos/hello
RUN ../gradlew --no-daemon clean installApp \
    && chmod +x ./build/install/hello/bin/q2
EXPOSE 5005 8000 8080 8086 9000
ENTRYPOINT ["/root/hello_jpos/hello/build/install/hello/bin/q2"]