## Hello, jPOS!

A tiny template to play around and understand venerable jPOS framework.

Clone this project in order to create your own jPOS based application.

We recommend that you install [Gradle](http://gradle.org/) in order to build your jPOS projects, but if you don't have it installed, you can use the Gradle wrapper scripts `gradlew` and `gradlew.bat`. In the following instructions, when we say `gradle` we really mean either your installed Gradle or one of the wrapper scripts (depending if you are on Unix or DOS based platforms).

### Build an IDEA project
````
gradle idea
````

### Install application in 'build/install' directory
````
gradle installApp
````
Installs application in `build/install` with everything you need to run jPOS. Once the directory is created, you can `cd build/install` and call `java -jar your-project-version.jar` or the `bin/q2` (or `q2.bat`) script available in the `bin` directory.

### List available Gradle tasks
````
gradle tasks
````

### Testing
#### Start container
Ensure docker is installed on your machine.

Run below from _project root directory_:
````bash
docker build --no-cache . # mind the "."
docker compose up
````
#### NetCat
Simple telnet interface can be used:
````bash
brew install netcat
nc localhost 8000
````
Sample request from below can then be pasted into terminal and compared with expected response.

#### Curl/Postman
Sample request can be used via Postman/curl using port `8086:
````bash
curl -i http://localhost:8086/api/echo \
-H "Content-Type: application/xml" \
-H "Accept: application/xml" \
--request POST \
--data "<isomsg> \
    <field id=\"0\" value=\"0800\"/> \
    <field id=\"02\" value=\"4111111111111111\"/> \
    <field id=\"11\" value=\"000001\"/> \
    <field id=\"41\" value=\"00000001\"/> \
    <field id=\"70\" value=\"301\"/> \
</isomsg>"
````

#### Sample request/response
Request:
````xml
<isomsg>
    <field id="0" value="0800"/>
    <field id="02" value="4111111111111111"/>
    <field id="11" value="000001"/>
    <field id="41" value="00000001"/>
    <field id="70" value="301"/>
</isomsg>
````
Response:
````xml
<isomsg>
  <field id="0" value="0810"/>
  <field id="2" value="4200000000000000"/>
  <field id="11" value="000001"/>
  <field id="35" value="SUCCESS"/>
  <field id="37" value="478525"/>
  <field id="38" value="574706"/>
  <field id="39" value="00"/>
  <field id="41" value="00000001"/>
  <field id="70" value="301"/>
</isomsg>
````

### Debugging
A remote debugger from IDE can be attached to port `5005`

A good place to start is the endpoint itself, i.e. `org.jpos.hello.services.Echo::echoPost` method.

### Database

A `postgreSQL` database will be created as part of container construction.

Applications like `dbeaver` or `pgadmin` can be used to access it on port `9876`.

Database credentials can be found in `docker-compose.yml`.

### Troubleshooting

Database can be deleted by removing `db/postgres-data` directory. It will be automatically recreated on next `docker compose up`.