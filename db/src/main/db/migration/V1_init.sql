--
-- Role: jpos; Superuser
--
CREATE ROLE jpos WITH
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	LOGIN
	REPLICATION
	BYPASSRLS
	CONNECTION LIMIT -1;

--
-- Name: tx; Type: TABLE; Schema: public; Owner: jpos; Tablespace:
--
CREATE TABLE tx (
    id uuid NOT NULL,
    type character(4),
    card character varying(18),
    txdata character varying(100),
    actioncode character(2)
);


ALTER TABLE tx OWNER TO jpos;