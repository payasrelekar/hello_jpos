package org.jpos.hello.services;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.webapp.WebAppContext;
import org.glassfish.jersey.servlet.ServletContainer;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.q2.QBeanSupport;

/**
 * Generate Jetty server config.
 */
public class Jetty extends QBeanSupport {

    private String config;
    private Server server;
    private Server internalServer;
    private String war;

    private String contextPath;

    private int intPort;
    private int extPort;

    private int maxThreads;
    private int minThreads;

    @Override
    public void initService() throws Exception {

        WebAppContext webapp = new WebAppContext();

        QueuedThreadPool threadPool = new QueuedThreadPool(maxThreads, minThreads);

        ServletHolder jerseyServlet = webapp.addServlet(ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(1);
        jerseyServlet.setInitParameter("javax.ws.rs.Application", "org.jpos.hello.services.App");

        webapp.setContextPath(contextPath);
        webapp.setWar(war);

        server = new Server(extPort);

        server.setHandler(webapp);

        WebAppContext webapp2 = new WebAppContext();

        ServletHolder jerseyServlet2 = webapp2.addServlet(ServletContainer.class, "/*");
        jerseyServlet2.setInitOrder(1);

        webapp2.setContextPath(contextPath);
        webapp2.setWar(war);

        internalServer = new Server(intPort);
        internalServer.setHandler(webapp2);

    }

    @Override
    public void startService() throws Exception {
        server.start();
        internalServer.start();
    }

    @Override
    public void stopService() throws Exception {
        server.stop();
        internalServer.stop();
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        super.setConfiguration(cfg);

        config = cfg.get("config", "");
        war = cfg.get("war", "");

        contextPath = cfg.get("context-path", "/api");
        intPort = cfg.getInt("internal-port", 9999);
        extPort = cfg.getInt("external-port", 8081);

        maxThreads = cfg.getInt("max-threads", 200);
        minThreads = cfg.getInt("min-threads", 10);
    }
}
