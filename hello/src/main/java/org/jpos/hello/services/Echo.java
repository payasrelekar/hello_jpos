package org.jpos.hello.services;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import org.jpos.transaction.TxnConstants;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static org.jpos.hello.util.Constants.*;
import static org.jpos.hello.util.ServiceUtils.getISOMsg;
import static org.jpos.hello.util.ServiceUtils.queryTxnMgr;

/**
 * HTTP service endpoints.
 */
@Consumes({MediaType.APPLICATION_XML})
@Path("/echo")
public class Echo {
    @POST
    @Produces({MediaType.APPLICATION_XML})
    public Response echoPost(
            @javax.ws.rs.core.Context UriInfo uriInfo,
            String payload
    ) throws ISOException {
        Context ctx = new Context();
        ctx.put(TXNNAME, getClass().getSimpleName() + "::echoPost");
        ctx.put(URI_INFO, uriInfo);
        ctx.put(PAYLOAD, payload);
        ctx.put(DESTINATION, "jPOS-AUTORESPONDER");

        ISOMsg msg = getISOMsg(payload);
        ctx.put(REQUEST, msg);

        ISOMsg result = queryTxnMgr(ctx);

        String resp;
        String errors = "";

        if (result == null) {
            errors += ctx.getString(TxnConstants.RC);

            if (!errors.equals("null"))
                resp = errors;
            else
                resp = "unexpected response";
        } else {
            resp = new String(result.pack());
        }
        return Response.ok(resp, MediaType.APPLICATION_XML)
                .status(Response.Status.OK)
                .location(((UriInfo) ctx.get(URI_INFO)).getAbsolutePath())
                .build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTransactions(
            @javax.ws.rs.core.Context UriInfo uriInfo
    ) {
        Context ctx = new Context();

        // TODO (exercise): implement a get API to fetch previously stored transactions

        return Response.ok(null, MediaType.APPLICATION_XML)
                .status(Response.Status.OK)
                .location(((UriInfo) ctx.get(URI_INFO)).getAbsolutePath())
                .build();
    }
}
