package org.jpos.hello.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ext.ContextResolver;

public class App extends ResourceConfig {

    public App() {
        super();
        register(JacksonFeature.class);
        register(new App.Resolver());
        packages("org.jpos.hello.services");
    }

    private static class Resolver implements ContextResolver<ObjectMapper> {
        final ObjectMapper defaultObjectMapper = createDefaultMapper();

        @Override
        public ObjectMapper getContext(Class<?> type) {
            return defaultObjectMapper;
        }

        private ObjectMapper createDefaultMapper() {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);

            return mapper;
        }
    }
}