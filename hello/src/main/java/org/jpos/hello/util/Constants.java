package org.jpos.hello.util;

public class Constants {
    public static final String PAYLOAD = "PAYLOAD";
    public static final String TXNNAME = "TXNNAME";
    public static final String REQUEST = "REQUEST";
    public static final String DESTINATION = "DESTINATION";
    public static final String TXNMGR = "TXNMGR";
    public static final String RESPONSE = "RESPONSE";
    public static final String URI_INFO = "URI_INFO";
}
