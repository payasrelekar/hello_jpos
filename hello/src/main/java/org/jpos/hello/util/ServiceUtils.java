package org.jpos.hello.util;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.XMLPackager;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.transaction.TxnConstants;

import static org.jpos.hello.util.Constants.RESPONSE;
import static org.jpos.hello.util.Constants.TXNMGR;

public class ServiceUtils {
    private ServiceUtils() {
        // util class
    }

    public static ISOMsg getISOMsg(String payload) throws ISOException {
        ISOMsg msg = new ISOMsg();
        msg.setPackager(new XMLPackager());
        msg.unpack(payload.getBytes());

        return msg;
    }

    public static ISOMsg queryTxnMgr(Context ctx) {
        SpaceFactory.getSpace().out(TXNMGR, ctx, 15000L);
        ISOMsg result = ctx.get(RESPONSE, 15000L);
        if (result == null) {
            ctx.put(TxnConstants.RC, "timeout");
        }
        return result;
    }
}
