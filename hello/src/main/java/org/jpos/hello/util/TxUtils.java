package org.jpos.hello.util;

import org.jpos.hello.entity.Tx;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.util.UUID;

public class TxUtils {
    private TxUtils() {
        // util class
    }

    public static Tx createTxFromISOMsg(ISOMsg msg) throws ISOException {
        Tx tx = new Tx();
        tx.setId(UUID.randomUUID());
        tx.setType(msg.getMTI());
        tx.setCard(msg.getString(2));
        tx.setTxData(msg.getString(35));
        tx.setActionCode(msg.getString(39));

        return tx;
    }
}
