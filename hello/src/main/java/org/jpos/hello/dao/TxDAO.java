package org.jpos.hello.dao;

import org.hibernate.query.Query;
import org.jpos.ee.DB;
import org.jpos.hello.entity.Tx;

import java.util.List;

public class TxDAO {
    DB db;
    public TxDAO(DB db) {
        this.db = db;
    }

    public void storeTx(Tx tx) {
        db.saveOrUpdate(tx);
    }

    private List<Tx> getTxList() {
        String queryStr = "select * from tx";

        Query query = db.session()
                .createNativeQuery(queryStr)
                .addEntity(Tx.class);

        return (List<Tx>) query.list();
    }
}
