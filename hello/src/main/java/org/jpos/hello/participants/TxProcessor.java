package org.jpos.hello.participants;

import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;

import java.io.Serializable;
import java.util.Random;

import static org.jpos.hello.util.Constants.REQUEST;
import static org.jpos.hello.util.Constants.RESPONSE;

public class TxProcessor implements TransactionParticipant, Configurable {
    Configuration cfg;

    @Override
    public int prepare(long id, Serializable context) {
        Context ctx = (Context) context;

        ISOMsg req = (ISOMsg) ctx.get(REQUEST);
        assert req != null;
        ISOMsg resp = (ISOMsg) req.clone();
        try {
            processRequest(req, resp);
        } catch (ISOException e) {
            throw new RuntimeException(e);
        }
        ctx.put(RESPONSE, resp);
        return PREPARED | NO_JOIN | READONLY;
    }

    private void processRequest(ISOMsg req, ISOMsg resp) throws ISOException {
        Random random = new Random (System.currentTimeMillis());
        resp.setResponseMTI();
        resp.set (37, Integer.toString(Math.abs(random.nextInt()) % 1000000));
        resp.set (38, Integer.toString(Math.abs(random.nextInt()) % 1000000));
        if ("000000009999".equals (req.getString (4))) {
            resp.set (35, "KABOOM");
            resp.set (39, "01");
        } else {
            resp.set (35, "SUCCESS");
            resp.set (39, "00");
        }
    }

    public void setConfiguration(Configuration cfg) {
        this.cfg = cfg;
    }
}
