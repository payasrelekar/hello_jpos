package org.jpos.hello.participants;

import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.ee.BLException;
import org.jpos.hello.dao.TxDAO;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;

import java.io.Serializable;

import static org.jpos.hello.util.Constants.REQUEST;
import static org.jpos.hello.util.Constants.RESPONSE;
import static org.jpos.hello.util.TxUtils.createTxFromISOMsg;

public class TxPersister implements TransactionParticipant, Configurable {
    Configuration cfg;

    @Override
    public int prepare(long id, Serializable context) {
        try {
            return doPrepare(id,context);
        } catch (Exception e) {
            return ABORTED;
        }
    }

    public int doPrepare(long id, Serializable context) throws Exception  {
        Context ctx = (Context) context;
        ISOMsg req = ctx.get(REQUEST);
        ISOMsg resp = ctx.get(RESPONSE);

        if(req ==null || resp == null) {
            throw new BLException("ERROR", "ERROR!");
        }

        persist(req, resp);

        return PREPARED | NO_JOIN | READONLY;
    }

    private void persist(ISOMsg... msgs) throws Exception {
        org.jpos.ee.DB.execWithTransaction(db -> {

            TxDAO dao = new TxDAO(db);
            for (ISOMsg msg : msgs) {
                dao.storeTx(createTxFromISOMsg(msg));
            }

            db.session().flush();
            db.session().clear();
            return null;
        });
    }

    public void setConfiguration(Configuration cfg) {
        this.cfg = cfg;
    }
}
