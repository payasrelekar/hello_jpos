package org.jpos.hello.entity;

import org.hibernate.annotations.DynamicInsert;
import org.jpos.ee.Cloneable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "tx")
@DynamicInsert
public class Tx extends Cloneable {

    @Column(name = "id")
    private UUID id;

    @Column(name = "type")
    private String type;

    @Column(name = "card")
    private String card;

    @Column(name = "txData")
    private String txData;

    @Column(name = "actioncode")
    private String actionCode;

    @Id
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getTxData() {
        return txData;
    }

    public void setTxData(String txData) {
        this.txData = txData;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        this.setId(null);
        return super.clone();
    }
}
